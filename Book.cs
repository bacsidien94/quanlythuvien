﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO; //hàm này để thao tác với file, vd ghi/đọc dữ liệu vào file
using System.Linq;


namespace Quanlythuvien
{
    class Book 
    {
        /* Yêu cầu theo đề bài thì object sách có thể có 7 thuộc tính như sau:
        mã sách, tên sách, tác giả, nhà xuất bản, giá sách, loại sách, mã ISBN 
        Có hai loại sách: sách tiếng Việt và sách Ngoại văn (có thêm ISBN)
        */
        // 7 thuộc tính của sách theo đề thầy yêu cầu
        public string bookID { get; set; }            //1. Mã sách
        public string bookName { get; set; }          //2. Tên sách
        public string bookAuthor { get; set; }        //3. Tác giả 
        public string bookPublisher { get; set; }     //4. Nhà xuất bản     
        public double bookPriceTag { get; set; }      //5. Giá sách
        public string isEN { get; set; }              //6. Loại sách
        public string bookISBN { get; set; }          //7. ISBN 

        // hàm này cho phép đổi giá trị thể hiện khi in ra màn hình cho đẹp thôi
        // lưu trong file là 1 hoặc 0
        // nhưng in ra thì đã được xử lý cho gần gũi với người dùng
        public static string convertENVN(string input)
        {
            string convert;
            if (input == "1")
            {
                convert = "Sách Ngoại Văn";
            }
            else { convert = "Sách Tiếng Việt"; }
            return convert;
        }

        // hàm này dùng để kiểm tra giá trị nhập vào có thích hợp dùng cho giá trị tiền hay không
        public static bool convertPrice(string input)
        {
            // hàm TryParse cho phép ép kiểu giá trị, 
            // nếu không ép được, thì trả về một giá trị mặc định ở out
            double temp;
            if (!double.TryParse(input, out temp) && input != "0")
            {
                Console.WriteLine("Vui lòng nhập giá tiền với chữ số tối đa 13 ký tự ");
                return false;
            }
            else { return true; }

        }

        // hàm này thực hiện chức năng thêm sách
        public static void addBook()
        {

            // khởi tạo một object dạng class Book
            // đại ý đây là một thực thể sách
            // thực thể này sẽ có 7 thuộc tính như trong class Book có mô tả
            // có thể trực tiếp gián giá trị cho các thuộc tính luôn
            // vd newbook.bookID = "abc gì đó";
            Book newbook = new Book();
            
            string inputprice; // biến nhận giá trị tiền

            Console.Write("Nhập Mã Sách: ");
            newbook.bookID = Console.ReadLine();
            // cho nhập giá trị một lần
            // khi giá trị đầu vàokhông thoả mãn điều kiện thì yêu cầu người dùng nhập lại 
            // kiểm tra mã sách đã tồn tại trong danh sách mylib hay chưa 
            // và ràng buộc không cho mã sách dài hơn 8 ký tự (mình tự thêm thôi)
            while (checkexistedbookID(newbook.bookID) || newbook.bookID.Length > 8)
            {    
                Console.Write("Vui lòng nhập mã sách khác tối đa 08 ký tự: ");
                newbook.bookID = Console.ReadLine();
            }

            //tương tự
            Console.Write("Nhập Tên Sách: "); newbook.bookName = Console.ReadLine();

            while (newbook.bookName.Length > 30)
            {
                Console.Write("Vui lòng nhập tên sách tối đa 30 ký tự: "); newbook.bookName = Console.ReadLine();
            }

            Console.Write("Nhập Tên Tác Giả: "); newbook.bookAuthor = Console.ReadLine();

            while (newbook.bookAuthor.Length > 30)
            {
                Console.Write("Vui lòng nhập tên tác giả tối đa 30 ký tự: "); newbook.bookAuthor = Console.ReadLine();
            }

            Console.Write("Nhập Nhà Xuất Bản: "); newbook.bookPublisher = Console.ReadLine();

            while (newbook.bookPublisher.Length > 20)
            {
                Console.Write("Vui lòng nhập nhà xuất bản tối đa 20 ký tự "); newbook.bookPublisher = Console.ReadLine();
            }

            Console.Write("Nhập Giá Sách: "); inputprice = Console.ReadLine();
            // kiểm tra giá trị nhập phải là giá trị số hay không
            // vì nhập chữ thì không hợp lệ để làm giá tiền
            // ràng buộc như vầy để tránh người dùng phá game
            // bắt nhập lại đến khi nào hợp lệ thì thôi
            while (!convertPrice(inputprice) || inputprice.Length > 13)
            {
                inputprice = Console.ReadLine();
            }
            newbook.bookPriceTag = double.Parse(inputprice);

            Console.WriteLine("Loại sách");
            Console.Write("Sách tiếng Việt - nhập (0) | sách Ngoại Văn - nhập (1): "); newbook.isEN = Console.ReadLine();
            // kiểm tra giá trị nhập bắt buộc phải là 0 hoặc 1
            while (newbook.isEN != "0" && newbook.isEN != "1")
            {
                Console.WriteLine("Vui lòng nhập loại sách hợp lệ");
                Console.Write("Sách tiếng Việt - nhập (0) | sách Ngoại Văn - nhập (1): "); newbook.isEN = Console.ReadLine();
            }
            // nếu là sách Tiếng Việt sẽ không có ISBN - Nếu là sách Ngoại Văn, yêu cầu nhập ISBN
            // ở đây nếu là 1 là sách ngoại văn, thì cho nhập ISBN
            // ISBN là một loại mã định danh cho sách, chống làm hàng giả, có thể xem thêm wiki để hiểu
            if (newbook.isEN == "1")
            {
                Console.Write("Nhập mã ISBN: "); newbook.bookISBN = Console.ReadLine();
                while (newbook.bookISBN.Length > 13)
                {
                    Console.Write("Vui lòng nhập mã ISBN tối đa 13 ký tự: "); newbook.bookISBN = Console.ReadLine();
                }
            }
            else {
                newbook.bookISBN = ""; //mặc định cho ISBN của sách là null
            }


            // tạo một biến string newrecord để lưu tất cả thông tin chi tiết sách 
            // thông tin này sẽ dùng là một bản ghi (record) và để lưu vào file 
            // dùng với dạng $ format cho gọn thay vì phải + từng chuỗi giá trị
            string newrecord = $"{newbook.bookID},{newbook.bookName},{newbook.bookAuthor},{newbook.bookPublisher},{newbook.bookPriceTag},{newbook.isEN},{newbook.bookISBN}";

            // mở file txt, đọc và lưu các bản ghi hiện có trong file vào một List<string> tên lines
            List<string> lines = System.IO.File.ReadAllLines(@"../../myLibrary.txt").ToList();
            // thêm bản ghi mới nhất vào lines
            lines.Add(newrecord);

            // tới đây biến lines sẽ có gì?
            // bao gồm các bản ghi thể hiện các sách đã có trong thư viện
            // và một bản ghi mới của sách ta mới tạo

            // và lưu lại vào file myLibrary.txt
            // dùng hàm WriteAllLines, tất cả dữ liệu hiện hữu trong myLibrary.txt sẽ bị ghi đề bằng thông tin mới từ biến lines
            File.WriteAllLines(@"../../myLibrary.txt", lines);

            // code chạy được tới đây, xem như ta đã hoàn tất việc thêm sách
            Console.WriteLine("\nThêm Sách Đã Hoàn Tất!\n");

        }

        // hàm này thực hiện chức năng in danh sách các sách hiện có trong file
        // tuy nhiên đề không yêu cầu, làm thêm cho đẹp thôi
        // hàm này được chạy sau khi người dùng hoàn tất một tác vụ cụ thể (thêm xoá sửa sách)
        // có ý nghĩa vừa kiểm tra lại kết quả vừa làm, vừa xem danh sách thư viện mới nhất luôn
        public static void listbook()
        {
            string filePath = @"../../myLibrary.txt";
            // tạo một biến dạng danh sách, biến này sẽ chứa các bản ghi đọc được từ file myLibrary.txt
            List<string> listbook = new List<string>();
            listbook = File.ReadAllLines(filePath).ToList();

            // giá trị bên cạnh mỗi slot có ý nghĩa tạo một placeholder cho giá trị đó với chiều dài x
            // vd {0,-8} nghĩa là, biến thứ 0 này sẽ được in ra trong 1 vùng rộng bằng 8 ký tự
            // giá trị -/+ cho phép giá trị align từ trái sang phải hay phải sang trái của placeholder này
            // dùng cách này để in danh sách ra theo hàng thẳng lối cho đẹp
       
            Console.WriteLine("{0,-8}{1,-30}{2,-30}{3,-20}{4,-13}{5,-18}{6,-18}", "Mã Sách", "Tên Sách", "Tên Tác Giả", "NXB", "Giá", "Ngôn Ngữ", "ISBN");
            // chạy vòng lặp
            // record là một bản ghi (1 dòng) trong danh sách listbook 
            foreach (string record in listbook)
            {
                // tách từng giá trị trong một dòng khi đọc lần lượt tới nó, xác định bằng dấu phẩy ,
                // và lưu vào một mảng dạng chuỗi
                // vậy ta có thể hiểu 1 dòng giá trị thông tin sách sẽ được lưu vào 1 mảng có 7 giá trị (ứng với 7 thuộc tính sách)
                string[] eachRecord = record.Split(',');

                // cho phép hiển thị loại tiền tệ Việt Nam khi hiển thị giá sách
                // string format C0 nghia la hiển thị giá trị dưới kiểu tiền tệ mà không có giá trị thập phân 
                // vì Việt Nam không cho phép hiện thập phân
                // IFormatProvider chọn loại CultureInfo cho Việt Nam vi-VN
                eachRecord[4] = double.Parse(eachRecord[4]).ToString("C0", CultureInfo.CreateSpecificCulture("vi-VN"));

                // và in từng giá trị trong mảng ra luôn, hãy lưu ý placeholder ở mỗi giá trị được chừa giống với tiêu đề
                // riêng ở eachRecord[5] ứng với loại sách, truyền giá trị của nó vào để cho hàm xử lý in ra loại sách tương ứng
                Console.WriteLine("{0,-8}{1,-30}{2,-30}{3,-20}{4,-13}{5,-18}{6,-18}", eachRecord[0], eachRecord[1], eachRecord[2], eachRecord[3], eachRecord[4], Book.convertENVN(eachRecord[5]), eachRecord[6]);
            }

        }
        
        // hàm cho phép tìm kiếm sách theo tên sách
        public static void inqBookbyName(string inputhere)
        {
            string filePath = @"../../myLibrary.txt";
            List<string> listbook = new List<string>();
            listbook = File.ReadAllLines(filePath).ToList();
            
            // khởi gán một list chứa các index kết quả khớp
            // vì dự trù trong trường hợp có nhiều hơn 1 kết quả khớp 
            List<int> indexmatched = new List<int>();
            for (int i = 0; i < listbook.Count; i++)
            {
                //record là mỗi bản ghi trong list , eachRecord là mỗi node của một bản ghi record
                string record = listbook[i];
                string[] eachRecord = record.Split(',');
                if (eachRecord[1].ToUpper().Contains(inputhere.ToUpper()))
                {
                    //mỗi kết quả khớp giá trị nhập vào sẽ được lưu vào List indexmatched
                    indexmatched.Add(i);
                }
                else
                {
                }
            }
            //nếu không có kết quả sách nào khớp với từ khoá tìm kiếm, in ra dòng kết quả như sau
            if (indexmatched.Count == 0) { Console.WriteLine("Không có sách trên"); }
            //nếu có kết quả sách khớp với từ khoá tìm kiếm, lần lượt in thông tin sách ra
            else
            {
                Console.WriteLine("Danh sách kết quả:");
                Console.WriteLine("{0,-8}{1,-30}{2,-30}{3,-20}{4,-13}{5,-18}{6,-18}", "Mã Sách", "Tên Sách", "Tên Tác Giả", "NXB", "Giá", "Ngôn Ngữ", "ISBN");
                //chạy một vòng lặp từ list các kết quả, đã được lưu vào indexmatched
                for (int x = 0; x < indexmatched.Count; x++)
                {
                    // record là mỗi bản ghi trong list , eachRecord là mỗi node của một bản ghi record
                    // dòng này hơi hại não
                    // biến này sẽ chỉ lưu giá trị của dòng nào trong danh sách.. mà được xem như là khớp với giá trị tìm kiếm 
                    string record = listbook[indexmatched[x]];
                    string[] eachRecord = record.Split(',');

                    eachRecord[4] = double.Parse(eachRecord[4]).ToString("C0", CultureInfo.CreateSpecificCulture("vi-VN"));
                    Console.WriteLine("{0,-8}{1,-30}{2,-30}{3,-20}{4,-13}{5,-18}{6,-18}", eachRecord[0], eachRecord[1], eachRecord[2], eachRecord[3], eachRecord[4], Book.convertENVN(eachRecord[5]), eachRecord[6]);
                }
            }

        }

        // hàm cho phép tìm kiếm sách theo mã sách, trả về nguyên cuốn sách đó luôn (dạng Book)
        // đây là một hướng khác cho chức năng tìm kiếm sách
        // hãy thử cách của bạn
        public static Book inqBookbyID(string bookID)
        {
            Book result = new Book();
            string filePath = @"../../myLibrary.txt";
            List<string> listbook = new List<string>();
            listbook = File.ReadAllLines(filePath).ToList();
            bool apply_chk = false;
            //string bookName = "";
            //string isEN = "";
            for (int i = 0; i < listbook.Count; i++)
            {
                //record là mỗi bản ghi trong list , eachRecord là mỗi node của một bản ghi record
                string record = listbook[i];
                string[] eachRecord = record.Split(',');
                //nếu node thứ nhất (có index 0) của record khớp với giá trị đầu vào
                //thì cho phép sửa 2 thuộc tính tên đọc giả / số điện thoại
                if (eachRecord[0] == bookID)
                {
                    apply_chk = true;
                    result.bookID = eachRecord[0];
                    result.bookName = eachRecord[1];
                    result.isEN = eachRecord[5];
                    break;
                }
                else
                {
                    apply_chk = false;
                }
            }
            // in kết quả tìm kiếm, nhớ là phải để ngoài vòng lặp for.. 
            // không thì mỗi lần chạy từng dòng bản ghi và nếu tìm không khớp nó đều in ra dòng này
            if (apply_chk == false)
            {
                Console.WriteLine("Không có mã sách trên");
                result.bookName = "";
            }
            return result;
        }

        // hàm này dùng để kiểm tra mã sách mới nhập vào.. đã tồn tại trong danh sách chưa
        public static bool checkexistedbookID(string bookID)
        {
            string filePath = @"../../myLibrary.txt";
            List<string> listbook = new List<string>();
            listbook = File.ReadAllLines(filePath).ToList();
            bool found_chk = false;

            for (int i = 0; i < listbook.Count; i++)
            {
                //record là mỗi bản ghi trong list , eachRecord là mỗi node của một bản ghi record
                string record = listbook[i];
                string[] eachRecord = record.Split(',');
                //nếu node thứ nhất (có index 0) của record khớp với giá trị đầu vào
                if (eachRecord[0] == bookID)
                {
                    found_chk = true;
                    break;
                }
                else
                {
                    found_chk = false;
                }
            }
            if (found_chk == true)
            {
                Console.WriteLine("Mã sách đã tồn tại");
            }
            else
            {
                //  Console.WriteLine("Mã sách hợp lệ"); 
            }
            return found_chk;
        }

        public static void editBookbyID(string bID)
        {
            string filePath = @"../../myLibrary.txt";
            List<string> listbook = new List<string>();
            listbook = File.ReadAllLines(filePath).ToList();
            bool edit_chk = false;
            for (int i = 0; i < listbook.Count; i++)
            {
                //record là mỗi bản ghi trong list , eachRecord là mỗi node của một bản ghi record
                string record = listbook[i];
                string[] eachRecord = record.Split(',');
                //nếu node thứ nhất (có index 0) của record khớp với giá trị đầu vào
                //thì cho phép sửa 4 thuộc tính tên sách/tên tác giả/nhà xuất bản/giá sách
                if (eachRecord[0] == bID)
                {
                    Console.WriteLine("Thực hiện việc sửa thông tin sách có mã {0}", bID);
                    // chỉ cho phép sửa 4 thuộc tính thôi .. nếu muốn sửa hết có lẽ người dùng nên xoá luôn sách rồi thêm mới đúng không
                    // có thể làm đơn giản nhu sau
                    //Console.Write("Nhập Tên Sách mới: "); eachRecord[1] = Console.ReadLine();
                    //Console.Write("Nhập Tên Tác Giả mới: "); eachRecord[2] = Console.ReadLine();
                    //Console.Write("Nhập Nhà Xuất Bản mới: "); eachRecord[3] = Console.ReadLine();
                    //Console.Write("Nhập Giá Sách mới: "); eachRecord[4] = Console.ReadLine();

                    // hoặc thêm các validation cần thiết
                    Console.Write("Nhập Tên Sách: "); eachRecord[1] = Console.ReadLine();

                    while (eachRecord[1].Length > 30)
                    {
                        Console.Write("Vui lòng nhập tên sách tối đa 30 ký tự: "); eachRecord[1] = Console.ReadLine();
                    }

                    Console.Write("Nhập Tên Tác Giả: "); eachRecord[2] = Console.ReadLine();

                    while (eachRecord[2].Length > 30)
                    {
                        Console.Write("Vui lòng nhập tên tác giả tối đa 30 ký tự: "); eachRecord[2] = Console.ReadLine();
                    }

                    Console.Write("Nhập Nhà Xuất Bản: "); eachRecord[3] = Console.ReadLine();

                    while (eachRecord[3].Length > 20)
                    {
                        Console.Write("Vui lòng nhập nhà xuất bản tối đa 20 ký tự: "); eachRecord[3] = Console.ReadLine();
                    }
                    string inputprice;
                    Console.Write("Nhập Giá Sách: "); inputprice = Console.ReadLine();
                    // kiểm tra giá trị nhập phải là giá trị số
                    while (!convertPrice(inputprice) || inputprice.Length > 13)
                    {
                        inputprice = Console.ReadLine();
                    }
                    eachRecord[4] = inputprice;

                    //lưu kết quả lại vào listbook[i]
                    listbook[i] = $"{eachRecord[0]},{eachRecord[1]},{eachRecord[2]},{eachRecord[3]},{eachRecord[4]},{eachRecord[5]},{eachRecord[6]}";
                    edit_chk = true;
                    Console.WriteLine("\nCập Nhật Thông Tin Sách Đã Hoàn Tất!\n");
                    break;
                }
                else { edit_chk = false; }
            }
            if (edit_chk == false) { Console.WriteLine("Không có mã sách trên"); return; }
            else
            {
                // update lại list book sau khi xoá
                // ghi lại vào file txt từ List<string> tên listbook
                File.WriteAllLines(@"../../myLibrary.txt", listbook);
                Console.WriteLine("Cập nhật thư viện sách mới nhất");
                Book.listbook();
                //Console.ReadLine();
            }
        }

        public static void delBookbyID(string bID)
        {
            bool found_chk = false;
            string filePath = @"../../myLibrary.txt";
            List<string> listbook = new List<string>();
            listbook = File.ReadAllLines(filePath).ToList();
            for (int i = 0; i < listbook.Count; i++)
            {
                //record là mỗi bản ghi trong list , eachRecord là mỗi node của một bản ghi record
                string record = listbook[i];
                string[] eachRecord = record.Split(',');
                if (eachRecord[0] == bID)
                {
                    listbook.Remove(record);
                    Console.WriteLine("Đã xoá sách có mã {0} ", bID);
                    found_chk = true;
                    break;
                }
                else { found_chk = false; }
            }
            if (found_chk == false)
            {
                Console.WriteLine("Không có mã sách trên");
            }
            // update lại list book sau khi xoá
            // ghi lại vào file txt từ List<string> tên listbook
            File.WriteAllLines(@"../../myLibrary.txt", listbook);
            Console.WriteLine("Cập nhật thư viện sách mới nhất");
            Book.listbook();
        }
    }
}
