﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quanlythuvien
{
    // các thiết lập cơ bản theo đề bài
    class GeneralCode
    {
        int freeDays; // số ngày cho mượn sách miễn phí
        // get - set để lấy giá trị hiện hữu của biến
        public int FreeRentingDay { get { return freeDays; } set { freeDays = value; } }

        double standardRateVN; // số tiền phạt quá hạn mượn sách - sách tiếng việt
        // get - set để lấy giá trị hiện hữu của biến
        public double StandardRateVN { get { return standardRateVN; } set { standardRateVN = value; } }

        double standardRateEN; // số tiền phạt quá hạn mượn sách - sách ngoại ngữ
        // get - set để lấy giá trị hiện hữu của biến
        public double StandardRateEN { get { return standardRateEN; } set { standardRateEN = value; } }

        // đây gọi là constructor, bắt buộc một khi người dùng gọi nó, phải khởi gán giá trị cho các thuộc tính mà nó yêu cầu luôn
        public GeneralCode(int _freeRentingDays, double _standardRateVN, double _standardRateEN)
        {
            this.freeDays = _freeRentingDays;
            this.standardRateVN = _standardRateVN;
            this.standardRateEN = _standardRateEN;
        }
    }
}
