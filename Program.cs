using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization; //hàm này để thể hiện giá tiền VND, hiện lên cho đẹp  

/* 
╔━━┓  ┏━━╦━━━━━╦━━┓  ╔━━┓  ╔━━━━━╗
║  ┗━━┛  ║  ┗━━╣  ┃  ║  ┃  ║ ^ ^ ║
║  ┏━━┓  ║  ┏━━╣  ┗━━╣  ┗━━╣  Y  ║
╚━━┛  ┗━━╩━━━━━╩━━━━━╩━━━━━╩━━━━━╝﻿ 
mình là bacsidien94, mình đã hiểu chắc chắn bạn cũng sẽ hiểu
facebook Tien Giap
*/

// trước khi chạy phải build solution nhé, Ctrl + Shift + B


namespace Quanlythuvien
{
    class Program
    {
        static void Main(string[] args)
        {

            // cho phép hiển thị UTF-8 string (Tiếng Việt) trong Console Application
            // nếu ko có UTF8, các dòng Console.WriteLine có tiếng việt sẽ banh hết, máy chỉ hiện ??? thôi
            // thử comment dòng này đi để test
            Console.OutputEncoding = System.Text.Encoding.UTF8;


            // chương trình truyền hình bắt đầu !!!

            // khởi tạo một số biến phụ trợ
            // selection để nhận giá trị lúc người dùng chọn chức năng theo menu
            // value là dạng interger của giá trị selection, xem dòng 86
            string selection; int value;

            // hàm này dùng để in màn hình tĩnh console theo yêu cầu của thầy, chứ không có xử lý tính toán gì hết
            // muốn biết nội dung của một hàm được viết như thế nào
            // đặt con trỏ vào hàm, nhấn F12 (Go to definition)
            mainScreen(); 
             
            // có thể dùng while hoặc do-while tuỳ sở thích
            // nên nhớ while sẽ kiểm tra thoả điều kiện trước khi chạy vào code trong nội dung {}
            // còn do-while sẽ chạy code trong nội dung ít nhất 1 lần, sau đó mới kiểm tra điều kiện

            do
            {
                // kiểm tra dữ liệu đầu vào
                // nếu thoả thì cho phép truyền giá trị này vào hàm tương ứng (1-11)
                // nên gán lại giá trị selection trong mỗi lần chạy vòng lặp, 
                // nếu để bên ngoài, vòng lặp sẽ chạy đến vô cực luôn sẽ không đạt yêu cầu
                Console.Write("Vui lòng chọn chức năng cần thực hiện: ");
                selection = Console.ReadLine();
                
                // một cách để kiểm tra dữ liệu đầu vào để cho thực tế hơn
                // cân nhắc dùng if và nhiều else if vì có nhiều thứ cần phải kiểm tra lắm
                // nên chú ý mọi thứ nhận từ console đầu vào, máy đều coi là dạng chuỗi string
                // do đó ta luôn phải ép về kiểu dữ liệu khác theo yêu cầu
              
                // nhập 0 thì kết thúc chương trình
                if (selection == "0")
                { 
                    Console.Write("Kết thúc chương trình");
                    Console.ReadLine();

                }
                // nếu không phải số thì yêu cầu nhập lại
                else if (!int.TryParse(selection, out value))
                {
                    Console.Write("Vui lòng chọn chức năng cần thực hiện: ");
                    Console.ReadLine();

                }
                // nếu là số nhưng không có trong chức năng từ 1-11 cũng yêu cầu nhập lại
                else if (int.Parse(selection) < 0 || int.Parse(selection) > 11)
                {
                    Console.Write("Vui lòng chọn chức năng cần thực hiện: ");
                    Console.ReadLine();

                }
                // điều kiện cuối cùng thì nên else
                // sau khi kiểm tra các thứ nhập liệu đầu vào, 
                // nếu không vấn đề gì thì cho phép gọi hàm chạy chương trình
                else
                {
                    value = int.Parse(selection);
                    libstart(value);
                    // hoặc có thể viết gọn như sau, nếu không muốn tạo thêm 1 biến khác
                    // libstart(int.Parse(selection));

                }

                // thêm một lần gọi hàm để sau khi làm hết các thao tác
                // lại chạy chương trình lại từ đầu cho người dùng làm chức năng khác
                mainScreen();
            } while (selection != "0"); // cứ nhập khác 0 là chạy hoài luôn


        }

        // hàm in màn hình chương trình console theo yêu cầu đề bài  
        static void mainScreen()
        {
            Console.WriteLine("---CHUONG TRINH QUAN LY THU VIEN---\n\n"
                         + "Danh sach cac chuc nang:\n"
                         + "\t1. Them sach\n"
                         + "\t2. Xoa sach\n"
                         + "\t3. Sua sach\n"
                         + "\t4. Tim kiem sach\n"
                         + "\t5. Them doc gia\n"
                         + "\t6. Xoa doc gia\n"
                         + "\t7. Sua doc gia\n"
                         + "\t8. Tim kiem doc gia\n"
                         + "\t9. Lap phieu muon sach\n"
                         + "\t10.Lap phieu tra sach\n"
                         + "\t11.Liet ke danh sach muon sach tre han\n"
                         + "\t0. Thoat\n");
        }

        // hàm gọi 11 chức năng của console
        // nhận một tham số đầu vào, và chạy hàm ở số tương ứng theo đề bài
        // có thể dùng if else if hoặc switch case tuỳ sở thích

        static void libstart(int slt)
        {

            switch (slt)
            {
                case 1:
                    {
                        Console.WriteLine("Chức năng thêm sách");
                        Book.addBook();
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Chức năng xoá sách");
                        Book.listbook();
                        Console.Write("Nhập Mã sách tương ứng với tên sách cần xóa: ");
                        //Console.Write("Vui long chon chuc nang can thuc hien: ");
                        string inputvar = Console.ReadLine();
                        Book.delBookbyID(inputvar);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Chức năng sửa sách");
                        Book.listbook();
                        Console.WriteLine("Nhập mã sách tương ứng với tên sách cần sửa: ");
                        string inputvar = Console.ReadLine();
                        Book.editBookbyID(inputvar);
                        break;
                    }
                case 4:
                    {
                        Console.WriteLine("Chức năng tìm kiếm sách");
                        //Book.listbook();
                        Console.Write("Nhập tên sách cần tìm: ");
                        string inputvar = Console.ReadLine();
                        Book.inqBookbyName(inputvar);
                        break;
                    }
                case 5:
                    {
                        Console.WriteLine("Chức năng thêm đọc giả");
                        // hàm này tương tự hàm tạo sách
                        // tự thực hành nhé
                        break;
                    }
                case 6:
                    {
                        Console.WriteLine("Chức năng xoá đọc giả");
                       // Reader.listreader();
                        Console.Write("Nhập mã đọc giả cần xoá: ");
                        string inputvar = Console.ReadLine();
                        // hàm này tương tự hàm xoá sách
                        // tự thực hành nhé
                        break;
                    }
                case 7:
                    {
                        Console.WriteLine("Chức năng sửa đọc giả");
                       // Reader.listreader();
                        Console.Write("Nhập mã đọc giả cần sửa: ");
                        string inputvar = Console.ReadLine();
                        // hàm này tương tự hàm sửa sách
                        // tự thực hành nhé
                        break;
                    }
                case 8:
                    {
                        Console.WriteLine("Chức năng tìm kiếm đọc giả");
                        Console.Write("Nhập tên đọc giả cần tìm: ");
                        string inputvar = Console.ReadLine();
                        // hàm này tương tự hàm tìm kiếm sách
                        // tự thực hành nhé
                        break;
                    }
                case 9:
                    {
                        Console.WriteLine("Chức năng lập phiếu mượn sách");
                        // hàm này tương tự hàm tạo sách
                        // tự thực hành nhé
                        break;
                    }
                case 10:
                    {
                        Console.WriteLine("Chức năng lập phiếu trả sách");
                        Console.Write("Nhập mã phiếu mượn sách cần trả: ");
                        string inputvar = Console.ReadLine();
                        // hàm này tương tự hàm sửa sách, với ý đồ là tìm phiếu mượn sách, rồi hiện một số thông tin của phiếu mượn, 
                        // nếu đồng ý trả sách thì trả tiền mượn quá hạn nếu có, 
                        // sau đó cập nhật trạng thái phiếu mượn thể hiện là đã trả
                        // tự thực hành nhé   
                        break;
                    }
                case 11:
                    {
                        Console.WriteLine("Chức năng liệt kê danh sách mượn sách trễ hạn");
                        // hàm này là hàm đọc dữ liệu dạng danh sách, 
                        // chạy lần lượt từng index của danh sách và in ra thôi
                        break;
                    }
               // có thể làm thêm default nếu cần

            }
        }
    }
    }

